package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ts.DAO.UserDAO;

/**
 * Servlet implementation class logInServlet
 */
@WebServlet("/login")
public class logInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		boolean userFound=false;
		if(email.equals("admin") && password.equals("admin")){
			//save the data in sesson before farwarding
			//1.get the session object
			//2.save in session object
			//-sessionAttribite("name",value);
			HttpSession session=request.getSession();
			session.setAttribute("logIn", email);
			RequestDispatcher rd= request.getRequestDispatcher("adminHome.jsp");
			rd.forward(request, response);
		}
		else{
		userFound=UserDAO.getUser(email, password);
		PrintWriter  pw=response.getWriter();
		
		if(userFound==true){
			pw.print("<h1 style=color:green;>Login successfull</h1>");
			RequestDispatcher rd= request.getRequestDispatcher("userHome.jsp");
			rd.forward(request, response);
		
		}
		else{
			pw.print("<h3 style=color:red;>log in failed try again</h1>");
			RequestDispatcher rd= request.getRequestDispatcher("facebooklogin.html");
			rd.include(request, response);
		}
		}
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
