package com.ts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.User;

import com.ts.DAO.UserDAO;

/**
 * Servlet implementation class GetAllUsersServlet
 */
@WebServlet("/getall")
public class GetAllUsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get data from session object and save it
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("logIn");
		
		ArrayList<com.ts.DAO.User> user=UserDAO.getAllUsers();
		PrintWriter out=response.getWriter();
		out.print("<span>"+email+"</span>");
		out.print("<center><h1 style=color:green;>All Users List</h1>");
		out.print("<table border=1>");
		out.print("<tr>");
		out.print("<td>EmpNO</td>");
		out.print("<td>EmpName</td>");
		out.print("<td>job</td>");
		out.print("</tr>");
		out.print("<tbody");
		for(com.ts.DAO.User u:user){
			out.print("<tr>");
			out.print("<td>"+u.getUserid()+"</td>");
			out.print("<td>"+u.getEmail()+"</td>");
			out.print("<td>"+u.getPassword()+"</td>");
			out.print("</tr>");
		}
		out.print("</tbody>");
		out.print("</table>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
