package com.ts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.DAO.Employee;
import com.ts.DAO.EmployeeDAO;

/**
 * Servlet implementation class GetAllEmployees
 */
@WebServlet("/getallemp")
public class GetAllEmployees extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Employee> emp=EmployeeDAO.getEmployees();
		PrintWriter out=response.getWriter();
		out.print("<table border=1>");
		out.print("<tr>");
		out.print("<td>EmpNO</td>");
		out.print("<td>EmpName</td>");
		out.print("<td>job</td>");
		out.print("<td>mgr</td>");
		out.print("<td>HireDate</td>");
		out.print("<td>sal</td>");
		out.print("<td>comm</td>");
		out.print("<td>deptNo</td>");
		out.print("<td>Email</td>");
		out.print("<td>password</td>");
		out.print("</tr>");
		out.print("<tbody");
		for(com.ts.DAO.Employee e:emp){
			out.print("<tr>");
			out.print("<td>"+e.getEmpNo()+"</td>");
			out.print("<td>"+e.getEmpName()+"</td>");
			out.print("<td>"+e.getJob()+"</td>");
			out.print("<td>"+e.getMgr()+"</td>");
			out.print("<td>"+e.getHireDate()+"</td>");
			out.print("<td>"+e.getSal()+"</td>");
			out.print("<td>"+e.getComm()+"</td>");
			out.print("<td>"+e.getDeptNo()+"</td>");
			out.print("<td>"+e.getEmail()+"</td>");
			out.print("<td>"+e.getPassword()+"</td>");
			out.print("</tr>");
		}
		out.print("</tbody>");
		out.print("</table>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
