package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminHomePage
 */
@WebServlet("/adminHome")
public class AdminHomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email=request.getParameter("email");
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		out.print("<span>");
		out.print(email);
		out.print("</span>");
		out.print("<center><h1 style=color:green;>Welcome to User HomePage</h1></center>");
		out.print("<center><a style=color:blue; href='AllUser.jsp'>Get all users</a>          ");
		out.print("<a style=color:blue; href='adminId'>Get user by id</a></center>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
