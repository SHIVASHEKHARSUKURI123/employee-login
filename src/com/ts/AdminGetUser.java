package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.User;

import com.ts.DAO.UserDAO;

/**
 * Servlet implementation class AdminGetUser
 */
@WebServlet("/adminGet")
public class AdminGetUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	      
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("id");
		com.ts.DAO.User user=UserDAO.getUserById(userId);
		HttpSession session =request.getSession();
		String email=(String)session.getAttribute("logIn");
		PrintWriter out=response.getWriter();
		if(user.getEmail()!=""){
		out.print("<span>"+email+"</span><br>");
		out.print("<center><h1 style=color:green;>Get User By Id</h1>");
		out.print("<table border=1>");
		out.print("<tr>");
		out.print("<td>EmpNO</td>");
		out.print("<td>EmpName</td>");
		out.print("<td>job</td>");
		out.print("</tr>");
		out.print("<tr>");
		out.print("<td>"+user.getUserid()+"</td>");
		out.print("<td>"+user.getEmail()+"</td>");
		out.print("<td>"+user.getPassword()+"</td>");
		out.print("</tr>");
		out.print("</table>");
	}
		else{
			out.print("<h1>User not found</h1>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
