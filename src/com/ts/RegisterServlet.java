package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.DAO.UserDAO;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String email=request.getParameter("email");
		String pass=request.getParameter("password");
//		String email=request.getParameter("email");
		//setting type of response object
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		int rows=0;
		//user should valid detilsotherwise show error message
		if(!email.equals("")&!pass.equals("")){
		//save inthe database-insert method on useDAO
			rows=UserDAO.insertuser(email, pass);
			if(rows==1){
				pw.print("user registered successfully");
			}
		}
		else{
			
			pw.print("<h1><styplease enter valid details</h1>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
