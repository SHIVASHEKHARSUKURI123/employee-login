package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AdminEnterId
 */
@WebServlet("/adminId")
public class AdminEnterId extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("logIn");
		response.setContentType("text/html");
//		String email=request.getParameter("email");
		PrintWriter out=response.getWriter();
		out.print("<form action='adminGet'>");
		out.print("<span>");
		out.print(email);
		out.print("</span>");
		out.print("<center><label>Enter user id</label><br></center>");
		out.print("<center><input type='number' name='id'></center><br>");
		out.print("<center><input type='submit'></center>");
		out.print("</form>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
