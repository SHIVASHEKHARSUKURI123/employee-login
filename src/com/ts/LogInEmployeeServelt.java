package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ts.DAO.EmployeeDAO;

/**
 * Servlet implementation class LogInEmployeeServelt
 */
@WebServlet("/loginemp")
public class LogInEmployeeServelt extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		if(email.equals("admin") && password.equals("admin")){
			HttpSession session=request.getSession();
			session.setAttribute("loginEmail", email);
			RequestDispatcher rd= request.getRequestDispatcher("EmployeeAdminHome.jsp");
			rd.forward(request, response);
		}

		else{
			boolean userFound=EmployeeDAO.getEmployee(email, password);
			PrintWriter pw=response.getWriter();
			if(userFound==true){
				//pw.print("<h1 style=color:green;>User Details Found</h1>");
				RequestDispatcher rd= request.getRequestDispatcher("EmployeeHome.jsp");
				rd.forward(request, response);
			}
			else{
				pw.print("<h1 style=color:red;>User not found</h1>");
				RequestDispatcher rd=request.getRequestDispatcher("EmployeeLogin.html");
				rd.include(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
