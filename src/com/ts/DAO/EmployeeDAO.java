package com.ts.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EmployeeDAO {
	public static int insertEmployee(int empNo,String name,String job,int mgr,
			 String HireDate,double sal,double comm,int deptNo,String email,String password){
		int rowsAffected=0;
		try{
		Class.forName("com.mysql.cj.jdbc.Driver");
	
		String dbUrl="jdbc:mysql://localhost:3306/fsd46";
		String dbUserName="root";
		String dbPassword="root";
		Connection con=null;
		con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String query="insert into emp1 values(?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt=con.prepareStatement(query);
		pstmt.setInt(1,empNo );
		pstmt.setString(2, name);
		pstmt.setString(3,job );
		pstmt.setInt(4,mgr );
		pstmt.setString(5,HireDate );
		pstmt.setDouble(6,sal );
		pstmt.setDouble(7,comm );
		pstmt.setInt(8,deptNo );
		pstmt.setString(9,email );
		pstmt.setString(10,password );
		System.out.println(pstmt);
		rowsAffected=pstmt.executeUpdate();
		
		}catch(SQLException ex){
			System.out.println("Excepton abv");
		}
		catch(ClassNotFoundException ex){
			System.out.println("Excepton");
		}
		System.out.println(rowsAffected);
		return rowsAffected;
	}
	public static boolean getEmployee(String email,String password){
		boolean userFound=false;
		try{
		Class.forName("com.mysql.cj.jdbc.Driver");
		String dbUrl="jdbc:mysql://localhost:3306/fsd46";
		String dbUserName="root";
		String dbPassword="root";
		Connection con=null;
		con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String query="select * from emp1 where email=? and password=?";
		PreparedStatement pstmt=con.prepareStatement(query);
		pstmt.setString(1, email);
		pstmt.setString(2, password);
		ResultSet rs=pstmt.executeQuery();
		if(rs.next()){
			userFound=true;
		}
		}catch(SQLException ex){
			System.out.println("Excepton");
		}
		catch(ClassNotFoundException ex){
			System.out.println("Excepton");
		}
		
		return userFound;
	}
	public static ArrayList<Employee> getEmployees(){
		ArrayList<Employee> emp=new ArrayList<Employee>();
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			String dbUrl="jdbc:mysql://localhost:3306/fsd46";
			String dbUserName="root";
			String dbPassword="root";
			Connection con=null;
			con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			String query="select * from emp1";
			PreparedStatement pstmt=con.prepareStatement(query);
			
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()){
				int empNo=rs.getInt(1);
				String empName=rs.getString(2);
				String job=rs.getString(3);
				int mgr=rs.getInt(4);
				String hireDate=rs.getString(5);
				double sal=rs.getDouble(6);
				double comm=rs.getDouble(7);
				int deptNo=rs.getInt(8);
				String email=rs.getString(9);
				String password=rs.getString(10);
				Employee e=new Employee(empNo,empName,job,mgr,hireDate,sal,comm,deptNo,email,password);
				emp.add(e);
			
			}
			}catch(SQLException ex){
				System.out.println("Excepton");
			}
			catch(ClassNotFoundException ex){
				System.out.println("Excepton");
			}
		return emp;
	}
	public static Employee getEmployee(int id){
//		ArrayList<Employee> emp=new ArrayList<Employee>();
		Employee e=null;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			String dbUrl="jdbc:mysql://localhost:3306/fsd46";
			String dbUserName="root";
			String dbPassword="root";
			Connection con=null;
			con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			String query="select * from emp1 where empNO=? ";
			PreparedStatement pstmt=con.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()){
				int empNo=rs.getInt(1);
				String empName=rs.getString(2);
				String job=rs.getString(3);
				int mgr=rs.getInt(4);
				String hireDate=rs.getString(5);
				double sal=rs.getDouble(6);
				double comm=rs.getDouble(7);
				int deptNo=rs.getInt(8);
				String email=rs.getString(9);
				String password=rs.getString(10);
				e=new Employee(empNo,empName,job,mgr,hireDate,sal,comm,deptNo,email,password);
			
			
			}
			}catch(SQLException ex){
				System.out.println("Excepton");
			}
			catch(ClassNotFoundException ex){
				System.out.println("Excepton");
			}
		return e;
	}
}
