package com.ts.DAO;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDAO {

public static int insertuser(String email,String password){
	int rowsAffected=0;
	try{
	Class.forName("com.mysql.cj.jdbc.Driver");
	
	String dbUrl="jdbc:mysql://localhost:3306/fsd46";
	String dbUserName="root";
	String dbPassword="root";
	Connection con=null;
	
	
	con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
	String query="insert into user(email,password) values(?,?)";
	
	
	
	PreparedStatement pstmt=con.prepareStatement(query);
	pstmt.setString(1, email);
	pstmt.setString(2, password);
	rowsAffected=pstmt.executeUpdate();
	
	}catch(SQLException ex){
		System.out.println("Excepton");
	}
	catch(ClassNotFoundException ex){
		System.out.println("Excepton");
	}
	
	return rowsAffected;
}
public static boolean getUser(String email,String password){
	boolean userFound=false;
	try{
	Class.forName("com.mysql.cj.jdbc.Driver");
	String dbUrl="jdbc:mysql://localhost:3306/fsd46";
	String dbUserName="root";
	String dbPassword="root";
	Connection con=null;
	con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
	String query="select * from user where email=? and password=?";
	PreparedStatement pstmt=con.prepareStatement(query);
	pstmt.setString(1, email);
	pstmt.setString(2, password);
	ResultSet rs=pstmt.executeQuery();
	if(rs.next()){
		userFound=true;
	}
	}catch(SQLException ex){
		System.out.println("Excepton");
	}
	catch(ClassNotFoundException ex){
		System.out.println("Excepton");
	}
	
	return userFound;
}
public static ArrayList<User> getAllUsers(){
	ArrayList<User> users=new ArrayList<User>();
	try{
	Class.forName("com.mysql.cj.jdbc.Driver");
	String dbUrl="jdbc:mysql://localhost:3306/fsd46";
	String dbUserName="root";
	String dbPassword="root";
	Connection con=null;
	con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
	String query="select * from user";
	PreparedStatement pstmt=con.prepareStatement(query);
	
	ResultSet rs=pstmt.executeQuery();
	while(rs.next()){
		int userid=rs.getInt(1);
		String email1=rs.getString(2);
		String password1=rs.getString(3);
		User user=new User(userid,email1,password1);
		users.add(user);
	}
	}catch(SQLException ex){
		System.out.println("Excepton");
	}
	catch(ClassNotFoundException ex){
		System.out.println("Excepton");
	}
	
	return users;
}
public static User getUserById(String userId){
	User user=null;
	try{
	Class.forName("com.mysql.cj.jdbc.Driver");
	String dbUrl="jdbc:mysql://localhost:3306/fsd46";
	String dbUserName="root";
	String dbPassword="root";
	Connection con=null;
	con=DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
	String query="select * from user where userId=?";
	PreparedStatement pstmt=con.prepareStatement(query);
	pstmt.setString(1, userId);
	ResultSet rs=pstmt.executeQuery();
	while(rs.next()){
		int userid=rs.getInt(1);
		String email1=rs.getString(2);
		String password1=rs.getString(3);
		user=new User(userid,email1,password1);
	}
	}catch(SQLException ex){
		System.out.println("Excepton");
	}
	catch(ClassNotFoundException ex){
		System.out.println("Excepton");
	}
	
	return user;
}


}
