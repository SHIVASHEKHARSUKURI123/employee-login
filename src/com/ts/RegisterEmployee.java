package com.ts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.DAO.EmployeeDAO;

/**
 * Servlet implementation class RegisterEmployee
 */
@WebServlet("/employee")
public class RegisterEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String empNO1=request.getParameter("id");
		int empNO=Integer.parseInt(empNO1);
		String empName=request.getParameter("name");
		String job=request.getParameter("job");
		String mgr1=request.getParameter("mgrid");
		int mgr=Integer.parseInt(mgr1);
		String date=request.getParameter("date");
		String sal1=request.getParameter("sal");
		double sal=Double.parseDouble(empNO1);
		String comm1=request.getParameter("comm");
		double comm=Double.parseDouble(comm1);
		String dept1=request.getParameter("dept");
		int dept=Integer.parseInt(dept1);
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		int rows=EmployeeDAO.insertEmployee(empNO, empName, job, mgr, date, sal, comm, dept, email, password);
		PrintWriter pw=response.getWriter();
		pw.print(rows);
		if(rows==1){
			pw.print("Registered successfull");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
