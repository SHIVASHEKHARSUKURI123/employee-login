<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="com.ts.DAO.Employee,com.ts.DAO.EmployeeDAO,java.util.ArrayList"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
//HttpSession se4=request.getSession();
String email4=(String)session.getAttribute("loginEmail");
ArrayList<Employee> e=EmployeeDAO.getEmployees();
%>
<h4><%=email4 %></h4>
<center>
<h1 style="color:green;">All Employees</h1>
<table border=1>
<tr >
<td>EmpNo</td>
<td>Ename</td>
<td>Job</td>
<td>MGR</td>
<td>Hiredate</td>
<td>sal</td>
<td>comm</td>
<td>DeptNo</td>
<td>Email</td>
<td>password</td>
</tr>

<%for(Employee emp1:e){ %>
<tr>
<td><%=emp1.getEmpNo() %></td>
<td><%=emp1.getEmpName() %></td>
<td><%=emp1.getJob() %></td>
<td><%=emp1.getMgr() %></td>
<td><%=emp1.getHireDate() %></td>
<td><%=emp1.getSal() %></td>
<td><%=emp1.getComm() %></td>
<td><%=emp1.getDeptNo() %></td>
<td><%=emp1.getEmail() %></td>
<td><%=emp1.getPassword() %></td>
</tr>
<%} %>
</table>

</center>
</body>
</html>